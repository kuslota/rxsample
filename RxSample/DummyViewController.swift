//
//  DummyViewController.swift
//  RxSample
//
//  Created by bo on 01.09.2020.
//  Copyright © 2020 bo. All rights reserved.
//

import UIKit

class DummyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

            
            
            //        // Subscription
            //        _ = observable.subscribe(onNext: { event in
            //            print(event)
            //        }, onError: { error in
            //            print(error)
            //        }, onCompleted: {
            //            print("finished")
            //        }) {
            //            print("disposed")
            //        }
            //
            //        _ = sequence.subscribe { event in
            //            print(event)
            //        }
            //
            //        // Disposing
            //        let array = ["a", "b", "c", "d"]
            //
            //        let bag = DisposeBag()
            //
            //        let arrayObservable = Observable<String>.from(array)
            //
            //        _ = arrayObservable.subscribe { event in
            //            print(event)
            //        }.disposed(by: bag)
            //
            // Operators
    //        let mappedSequence = sequence.map { $0 * 2}
    //
    //        let filterredSequence = sequence.filter { $0 % 2 == 0}
    //
    //        let filterredAndMapped = sequence
    //            .filter {$0 > 5}
    //            .map { $0 * 3}
    //
    //        let distinctedSequence = sequence.distinctUntilChanged()
    //
    //        let lastThree = sequence.takeLast(3)
    //
    //        // Creates something similar to timer
    //        let interval = Observable<Int>.interval(1, scheduler: MainScheduler.instance)
    //
    //        // Listen for changes oncein 2 seconds
    //        let throttleObservable = interval.throttle(DispatchTimeInterval.seconds(2), scheduler: MainScheduler.instance)
    //
    //        // Listens for changes if the last change was more than 500 ms ago
    //        let debounceObservable = interval.debounce(DispatchTimeInterval.milliseconds(500), scheduler: MainScheduler.instance)
    //
    //        _ = debounceObservable.subscribe { event in
    //            print(event)
    //        }

}
