//
//  ViewController.swift
//  RxSample
//
//  Created by bo on 01.09.2020.
//  Copyright © 2020 bo. All rights reserved.
//

import UIKit
import RxSwift

class ViewModel {
    let subject = PublishSubject<String>()
    private var counter = 0
    
    func incrementionRequested() {
        counter += 1;
        subject.onNext(String(counter))
    }
}

class ViewController: UIViewController {

    let viewModel = ViewModel()
    let bag = DisposeBag()
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func button(_ sender: UIButton) {
        viewModel.incrementionRequested()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.subject.subscribe(onNext: { num in
            self.label.text = num
        }).disposed(by: bag)
    }
}

